<?php

declare(strict_types=1);

namespace PcmtFhirBundle\Service\Helper;

use Akeneo\Pim\Enrichment\Component\Product\Model\Product;
use Akeneo\Pim\Enrichment\Component\Product\Model\ProductModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

interface FHIRMappingKey
{
    public const Identifier = 'identifier';

    public const Description = 'description';

    public const Name = 'name';

    public const Preferred = 'preferred';

    public const TradeName = 'trade-name';

    public const BaseUnit = 'baseUnit';

    public const NetContent = 'netContent';
}

class FHIR5MapHelper
{
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var ManagerInterface
     */
    private $entityManager;

    public function __construct(
        UrlGeneratorInterface $router,
        EntityManagerInterface $entityManager
    ) {
        $this->router = $router;
        $this->entityManager = $entityManager;
    }

    public static function mapIdentifier(
        string $attribute_route,
        string $code,
        string $product_model_route,
        array $values
    ): array {
        return [
            'type' => [
                'coding' => [
                    'system' => $attribute_route,
                    'code' => $code,
                    'display' => $code,
                ],
                'text' => $code,
            ],
            'system' => $product_model_route,
            'value' => $values[$code][0]['data'],
        ];
    }

    public function mapDescription(string $code, array $values): array
    {
        return [
            'description' => $values[$code][0]['data'],
        ];
    }

    public function mapName(string $attribute_route, string $code, array $values, string $nameTypeCode): array
    {
        return [
            'nameType' => [
                'system' => $attribute_route,
                'code' => $nameTypeCode,
                'display' => $values[$code][0]['data'],
            ],
            'name' => $values[$code][0]['data'],
        ];
    }

    public function mapCodableCode(array $codableConcepts, array $values, string $nameTypeCode): array
    {
        $codableCodes = [];
        foreach ($codableConcepts as $codableConcept) {
            $codableCodes[] = [
                'system' => $codableConcept['system_url'],
                'code' => $codableConcept['codable_code'],
                'display' => $values[$nameTypeCode][0]['data'],
            ];
        }
        return $codableCodes;
    }

    public function mapAssociations(array $associations): array
    {
        $mappedAssociationTypes = [];
        foreach ($associations as $key => $value) {
            $mappedAssociationTypes = array_merge($mappedAssociationTypes, $this->transformArray($value, $key));
        }
        return $mappedAssociationTypes;
    }

    public function mapCategories(array $categories): array
    {
        $mappedCategories = [];

        foreach ($categories as $category) {
            $category_route = $this->router->generate(
                'pim_api_category_get',
                [
                    'code' => $category->getCode(),
                ],
                UrlGeneratorInterface::ABSOLUTE_URL
            );

            $mappedCategories[] = [
                'coding' => [
                    'system' => $category_route,
                    'code' => $category->getCode(),
                    'display' => $category->getLabel(),
                ],
                'text' => $category->getLabel(),
            ];
        }
        return $mappedCategories;
    }

    public function transformArray($associationValues, $associationType)
    {
        $mappedAssociations = [];
        $supportedAssociations = ['products', 'product_models'];

        $associationsTypeCodings = [
            'system' => $this->router->generate(
                'pim_api_association_type_list',
                [],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
            'code' => $associationType,
            'display' => ucfirst(strtolower((string) $associationType)),
        ];

        foreach ($associationValues as $key => $associations) {
            if (in_array($key, $supportedAssociations, true)) {
                foreach ($associations as $association) {
                    $mappedAssociations[] = [
                        'associationType' => [
                            'coding' => $associationsTypeCodings,
                            'text' => ucfirst(strtolower((string) $associationType)),
                        ],
                        'relatedItem' => [
                            'reference' => $this->getAssociationUrls($association, $key),
                            'type' => 'InventoryItem',
                        ],
                        'quantity' => [
                            'numerator' => 1,
                            'denominator' => 1,
                        ],
                    ];
                }
            }
        }
        return $mappedAssociations;
    }

    private function getAssociationUrls($association, $modelType): string
    {
        $inventoryItemUuid = '';
        if ($modelType === 'products') {
            $productRepository = $this->entityManager->getRepository(Product::class);
            $inventoryItemUuid = $productRepository->findOneBy([
                'identifier' => $association,
            ]);
        } elseif ($modelType = 'product_models') {
            $productModelRepository = $this->entityManager->getRepository(ProductModel::class);
            $inventoryItemUuid = $productModelRepository->findOneById([
                'code' => $association,
            ]);
        }
        return 'GET ' . $this->router->generate('pim_fhir5_api_inventory_item_get', [
            'code' => $inventoryItemUuid->inventory_item_uuid,
        ], UrlGeneratorInterface::ABSOLUTE_URL);
    }
}
