<?php

/**
 * Copyright (c) 2020, VillageReach
 * Licensed under the Non-Profit Open Software License version 3.0.
 * SPDX-License-Identifier: NPOSL-3.0
 */

declare(strict_types=1);

namespace PcmtDraftBundle\Tests\Entity;

use PcmtFhirBundle\Entity\FhirMapping;
use PHPUnit\Framework\TestCase;

class FhirMappingTest extends TestCase
{
    public function testAddingNewFhirMapping(): void
    {
        $fhirMapping = new FhirMapping();

        $fhirMapping->setCode('ABC');

        $this->assertEquals('ABC', $fhirMapping->getCode());
    }
}
