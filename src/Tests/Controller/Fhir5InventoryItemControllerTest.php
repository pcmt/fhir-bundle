<?php

declare(strict_types=1);

namespace PcmtFhirBundle\Tests\Controller;

use Akeneo\Connectivity\Connection\Domain\Settings\Model\Read\User;
use Akeneo\Pim\Enrichment\Component\Product\Model\Product;
use Akeneo\Pim\Enrichment\Component\Product\Model\ProductModel;
use Akeneo\Pim\Enrichment\Component\Product\ProductModel\Query\GetConnectorProductModels;
use Akeneo\Pim\Enrichment\Component\Product\Query\GetConnectorProducts;
use Akeneo\Pim\Enrichment\Component\Product\Query\ProductQueryBuilderFactoryInterface;
use Akeneo\Pim\Enrichment\Component\Product\Repository\ProductModelRepositoryInterface;
use Akeneo\Pim\Enrichment\Component\Product\Repository\ProductRepositoryInterface;
use Akeneo\Tool\Component\Api\Pagination\PaginatorInterface;
use Akeneo\UserManagement\Bundle\Context\UserContext;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use FOS\OAuthServerBundle\Model\TokenInterface;
use Oro\Bundle\DataGridBundle\Datagrid\DatagridInterface;
use Oro\Bundle\DataGridBundle\Datagrid\ManagerInterface;
use PcmtFhirBundle\Controller\ExternalApi\Fhir5InventoryItemController;
use PcmtFhirBundle\Normalizer\ExternalApi\ConnectorR5InventoryItemNormalizer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class Fhir5InventoryItemControllerTest extends TestCase
{
    protected $pqbFactory;

    protected $offsetPaginator;

    protected $tokenStorage;

    protected $entityRepository;

    protected $fhir5InventoryItemNormalizer;

    protected $managerInterface;

    protected $normalizer;

    protected $productRepository;

    protected $userContext;

    protected $getConnectorProducts;

    protected $getConnectorProductModels;

    protected $entityManager;

    protected function setUp(): void
    {
        $this->pqbFactory = $this->createMock(ProductQueryBuilderFactoryInterface::class);
        $this->offsetPaginator = $this->createMock(PaginatorInterface::class);
        $this->tokenStorage = $this->createMock(TokenStorageInterface::class);
        $this->entityRepository = $this->createMock(EntityRepository::class);
        $this->fhir5InventoryItemNormalizer = $this->createMock(ConnectorR5InventoryItemNormalizer::class);
        $this->managerInterface = $this->createMock(ManagerInterface::class);
        $this->normalizer = $this->createMock(NormalizerInterface::class);
        $this->productRepository = $this->createMock(ProductRepositoryInterface::class);
        $this->userContext = $this->createMock(UserContext::class);
        $this->getConnectorProducts = $this->createMock(GetConnectorProducts::class);
        $this->getConnectorProductModels = $this->createMock(GetConnectorProductModels::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
    }

    public function testListActionReturnsValidJsonResponseWithDefaultParameters()
    {
        $controller = new Fhir5InventoryItemController(
            $this->pqbFactory,
            $this->offsetPaginator,
            $this->tokenStorage,
            [],
            $this->entityRepository,
            $this->fhir5InventoryItemNormalizer,
            $this->managerInterface,
            $this->normalizer,
            $this->productRepository,
            $this->userContext,
            $this->getConnectorProducts,
            $this->getConnectorProductModels,
            $this->entityManager
        );

        $request = new Request();

        $grid = $this->createMock(DatagridInterface::class);
        $this->managerInterface->method('getDatagrid')
            ->willReturn($grid);

        $this->fhir5InventoryItemNormalizer->method('normalizeConnectorInventory')
            ->willReturn([[
                'id' => 'test-id',
                'type' => 'test-document-type',
            ]]);

        $paginatedData = [
            '_links' => [
                'self' => [
                    'href' => 'http://example.com/self',
                ],
            ],
            '_embedded' => [
                'items' => [
                    [
                        '_links' => [
                            'self' => [
                                'href' => 'http://localhost:8080/api/fhir/r5/InventoryItem/2cc5682a-1b57-4bb3-91f6-41dd5e6309ad',
                            ],
                        ],
                        'resourceType' => 'InventoryItem',
                        'id' => '2cc5682a-1b57-4bb3-91f6-41dd5e6309ad',
                        'code' => 'R_842',
                        'identifier' => [],
                        'text' => [
                            'status' => 'generated',
                            'div' => '<div xmlns="http://www.w3.org/1999/xhtml"><div class="hapiHeaderText"> <b>R_842 </b></div></div>',
                        ],
                        'name' => null,
                        'status' => 'active',
                        'category' => [
                            [
                                'coding' => [
                                    'system' => 'http://localhost:8080/api/rest/v1/categories/MASTER_DATA',
                                    'code' => 'MASTER_DATA',
                                    'display' => 'Master and Mapping Data',
                                ],
                                'text' => 'Master and Mapping Data',
                            ],
                        ],
                        'description' => [],
                        'association' => [],
                    ],
                ],
            ],
        ];
        $this->offsetPaginator->method('paginate')
            ->willReturn($paginatedData);

        $response = $controller->listAction($request);

        $this->assertInstanceOf(JsonResponse::class, $response);

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('resourceType', $data);
        $this->assertArrayHasKey('type', $data);
        $this->assertArrayHasKey('timestamp', $data);
        $this->assertArrayHasKey('total', $data);
        $this->assertArrayHasKey('total', $data);
        $this->assertArrayHasKey('link', $data);
        $this->assertArrayHasKey('entry', $data);

        $this->assertIsArray($data['entry']);

        foreach ($data['entry'] as $entry) {
            $this->assertArrayHasKey('fullUrl', $entry);
            $this->assertEquals(
                'http://localhost:8080/api/fhir/r5/InventoryItem/2cc5682a-1b57-4bb3-91f6-41dd5e6309ad',
                $entry['fullUrl']
            );

            $this->assertArrayHasKey('resource', $entry);
            $this->assertIsArray($entry['resource']);

            $resource = $entry['resource'];
            $this->assertArrayHasKey('resourceType', $resource);
            $this->assertEquals('InventoryItem', $resource['resourceType']);

            $this->assertArrayHasKey('id', $resource);
            $this->assertEquals('2cc5682a-1b57-4bb3-91f6-41dd5e6309ad', $resource['id']);
        }
        $this->assertArrayHasKey('fullUrl', $entry);
    }

    public function testGetInventoryItemThrowsNotFoundHttpException()
    {
        // Mock token and user
        $token = $this->createMock(TokenInterface::class);
        $user = $this->createMock(User::class);
        $token->method('getUser')
            ->willReturn($user);
        $this->tokenStorage->method('getToken')
            ->willReturn($token);

        // Mock repositories
        $productRepository = $this->createMock(ProductRepositoryInterface::class);
        $productModelRepository = $this->createMock(ProductModelRepositoryInterface::class);

        // Mock EntityManager to return repositories
        $this->entityManager->method('getRepository')
            ->willReturnMap([[Product::class, $productRepository], [ProductModel::class, $productModelRepository]]);

        $productRepository->method('findOneBy')
            ->willReturn(null);
        $productModelRepository->method('findOneBy')
            ->willReturn(null);

        $controller = new Fhir5InventoryItemController(
            $this->pqbFactory,
            $this->offsetPaginator,
            $this->tokenStorage,
            [],
            $this->entityRepository,
            $this->fhir5InventoryItemNormalizer,
            $this->managerInterface,
            $this->normalizer,
            $this->productRepository,
            $this->userContext,
            $this->getConnectorProducts,
            $this->getConnectorProductModels,
            $this->entityManager
        );

        $request = new Request();

        // Assert that the NotFoundHttpException is thrown
        $this->expectException(NotFoundHttpException::class);
        $this->expectExceptionMessage('InventoryItem with resource inventory-item-code could not be found.');

        // Call the getInventoryItem method
        $controller->getInventoryItem($request, 'inventory-item-code');
    }
}
