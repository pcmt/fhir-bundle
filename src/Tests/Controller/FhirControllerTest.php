<?php

declare(strict_types=1);

namespace PcmtFhirBundle\Tests\Controller;

use Doctrine\ORM\EntityManagerInterface;
use PcmtFhirBundle\Controller\FhirController;
use PcmtFhirBundle\Entity\FhirMapping;
use PcmtFhirBundle\Repository\FhirMappingRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FhirControllerTest extends TestCase
{
    protected $entityManagerMock;

    protected $fhirMappingRepositoryMock;

    protected $fhirController;

    protected function setUp(): void
    {
        $this->entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $this->fhirMappingRepositoryMock = $this->createMock(FhirMappingRepository::class);
        $this->fhirController = new FhirController($this->entityManagerMock, $this->fhirMappingRepositoryMock);
    }

    public function testCreateAction(): void
    {
        $requestMock = $this->createMock(Request::class);
        $requestMock->method('getContent')
            ->willReturn(
                '{"code": "testCode", "type": "testType", "mapping": "testMapping","codable_concepts":[{"system_url": "test.come", "codable_code": "test code"}],"is_fhir5":false}'
            );

        $this->fhirMappingRepositoryMock->method('findOneBy')
            ->willReturn(null);
        $this->entityManagerMock->expects($this->once())
            ->method('persist');
        $this->entityManagerMock->expects($this->once())
            ->method('flush');

        $response = $this->fhirController->createAction($requestMock);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $responseData = json_decode($response->getContent(), true);
        $this->assertTrue($responseData['success']);
        $this->assertEmpty($responseData['error']);
    }

    public function testUpdateAction(): void
    {
        $requestMock = $this->createMock(Request::class);
        $requestMock->method('getContent')
            ->willReturn(
                '{"code": "testCode", "type": "testType", "mapping": "testMapping","codable_concepts":[{"system_url": "test.come", "codable_code": "test code"}],"is_fhir5":false}'
            );
        $fhirMapping = new FhirMapping();
        $fhirMapping->setCode('testCode');
        $fhirMapping->setType('testType');

        $this->fhirMappingRepositoryMock->method('findOneBy')
            ->willReturn($fhirMapping);
        $this->entityManagerMock->expects($this->once())
            ->method('persist');
        $this->entityManagerMock->expects($this->once())
            ->method('flush');

        $response = $this->fhirController->createAction($requestMock);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $responseData = json_decode($response->getContent(), true);
        $this->assertTrue($responseData['success']);
        $this->assertEmpty($responseData['error']);
    }

    public function testCurrentMappingAction(): void
    {
        $requestMock = $this->createMock(Request::class);
        $requestMock->method('getContent')
            ->willReturn(
                '{"code": "testCode", "type": "testType","codable_concepts":[{"system_url": "test.come", "codable_code": "test code"}],"is_fhir5":false}'
            );
        $fhirMapping = new FhirMapping();
        $fhirMapping->setMapping('testMapping');

        $this->fhirMappingRepositoryMock->method('findOneBy')
            ->willReturn($fhirMapping);

        $response = $this->fhirController->currentMappingAction($requestMock);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $responseData = json_decode($response->getContent(), true);
        $this->assertTrue($responseData['found']);
        $this->assertEquals('testMapping', $responseData['mapping']);
    }
}
