<?php

declare(strict_types=1);

namespace PcmtFhirBundle\Controller\ExternalApi;

use Akeneo\Pim\Enrichment\Bundle\Controller\ExternalApi\ProductModelController;
use Akeneo\Pim\Enrichment\Bundle\EventSubscriber\ProductModel\OnSave\ApiAggregatorForProductModelPostSaveEventSubscriber;
use Akeneo\Pim\Enrichment\Component\Product\Command\ProductModel\RemoveProductModelHandler;
use Akeneo\Pim\Enrichment\Component\Product\Connector\ReadModel\ConnectorProductModelList;
use Akeneo\Pim\Enrichment\Component\Product\Connector\UseCase\ListProductModelsQuery;
use Akeneo\Pim\Enrichment\Component\Product\Connector\UseCase\ListProductModelsQueryHandler;
use Akeneo\Pim\Enrichment\Component\Product\Connector\UseCase\ListProductsQueryHandler;
use Akeneo\Pim\Enrichment\Component\Product\Connector\UseCase\Validator\ListProductModelsQueryValidator;
use Akeneo\Pim\Enrichment\Component\Product\Connector\UseCase\Validator\ListProductsQueryValidator;
use Akeneo\Pim\Enrichment\Component\Product\Grid\Query;
use Akeneo\Pim\Enrichment\Component\Product\Model\Product;
use Akeneo\Pim\Enrichment\Component\Product\Normalizer\ExternalApi\ConnectorProductModelNormalizer;
use Akeneo\Pim\Enrichment\Component\Product\ProductModel\Filter\AttributeFilterInterface;
use Akeneo\Pim\Enrichment\Component\Product\ProductModel\Query\GetConnectorProductModels;
use Akeneo\Pim\Enrichment\Component\Product\Query\ProductQueryBuilderFactoryInterface;
use Akeneo\Tool\Bundle\ApiBundle\Cache\WarmupQueryCache;
use Akeneo\Tool\Bundle\ApiBundle\Documentation;
use Akeneo\Tool\Bundle\ApiBundle\Stream\StreamResourceResponse;
use Akeneo\Tool\Component\Api\Exception\DocumentedHttpException;
use Akeneo\Tool\Component\Api\Exception\InvalidQueryException;
use Akeneo\Tool\Component\Api\Pagination\PaginationTypes;
use Akeneo\Tool\Component\Api\Pagination\PaginatorInterface;
use Akeneo\Tool\Component\StorageUtils\Factory\SimpleFactoryInterface;
use Akeneo\Tool\Component\StorageUtils\Repository\IdentifiableObjectRepositoryInterface;
use Akeneo\Tool\Component\StorageUtils\Saver\SaverInterface;
use Akeneo\Tool\Component\StorageUtils\Updater\ObjectUpdaterInterface;
use Akeneo\UserManagement\Component\Model\UserInterface;
use Doctrine\ORM\EntityRepository;
use Elasticsearch\Common\Exceptions\BadRequest400Exception;
use Elasticsearch\Common\Exceptions\ServerErrorResponseException;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Oro\Bundle\SecurityBundle\SecurityFacade;
use PcmtFhirBundle\Normalizer\ExternalApi\ConnectorProductModelNormalizer as FhirConnectorProductModelNormalizer;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Webmozart\Assert\Assert;

/**
 * Copyright (c) 2022, VillageReach
 * Licensed under the Non-Profit Open Software License version 3.0.
 * SPDX-License-Identifier: NPOSL-3.0
 */
class FhirProductModelController extends ProductModelController
{
    protected ProductQueryBuilderFactoryInterface $pqbFactory;

    protected ProductQueryBuilderFactoryInterface $pqbSearchAfterFactory;

    protected NormalizerInterface $normalizer;

    protected IdentifiableObjectRepositoryInterface $channelRepository;

    protected PaginatorInterface $offsetPaginator;

    protected PaginatorInterface $searchAfterPaginator;

    protected array $apiConfiguration;

    protected ObjectUpdaterInterface $updater;

    protected SimpleFactoryInterface $factory;

    protected SaverInterface $saver;

    protected UrlGeneratorInterface $router;

    protected ValidatorInterface $productModelValidator;

    protected AttributeFilterInterface $productModelAttributeFilter;

    protected IdentifiableObjectRepositoryInterface $productModelRepository;

    protected StreamResourceResponse $partialUpdateStreamResource;

    private ListProductModelsQueryValidator $listProductModelsQueryValidator;

    private ListProductModelsQueryHandler $listProductModelsQueryHandler;

    private ConnectorProductModelNormalizer $connectorProductModelNormalizer;

    private GetConnectorProductModels $getConnectorProductModels;

    private TokenStorageInterface $tokenStorage;

    private ApiAggregatorForProductModelPostSaveEventSubscriber $apiAggregatorForProductModelPostSave;

    private WarmupQueryCache $warmupQueryCache;

    private LoggerInterface $logger;

    private LoggerInterface $apiProductAclLogger;

    private SecurityFacade $security;

    private RemoveProductModelHandler $removeProductModelHandler;

    private ValidatorInterface $validator;

    /**
     * @var EntityRepository
     */
    private $entityRepository;

    /**
     * @var FhirConnectorProductModelNormalizer
     */
    private $fhirConnectorProductModelNormalizer;

    /**
     * @var ListProductsQueryValidator
     */
    private $listProductsQueryValidator;

    /**
     * @var ListProductsQueryHandler
     */
    private $listProductsQueryHandler;

    public function __construct(
        ProductQueryBuilderFactoryInterface $pqbFactory,
        ProductQueryBuilderFactoryInterface $pqbSearchAfterFactory,
        NormalizerInterface $normalizer,
        IdentifiableObjectRepositoryInterface $channelRepository,
        PaginatorInterface $offsetPaginator,
        PaginatorInterface $searchAfterPaginator,
        ObjectUpdaterInterface $updater,
        SimpleFactoryInterface $factory,
        SaverInterface $saver,
        UrlGeneratorInterface $router,
        ValidatorInterface $productModelValidator,
        AttributeFilterInterface $productModelAttributeFilter,
        IdentifiableObjectRepositoryInterface $productModelRepository,
        StreamResourceResponse $partialUpdateStreamResource,
        ListProductModelsQueryValidator $listProductModelsQueryValidator,
        ListProductModelsQueryHandler $listProductModelsQueryHandler,
        ConnectorProductModelNormalizer $connectorProductModelNormalizer,
        GetConnectorProductModels $getConnectorProductModels,
        TokenStorageInterface $tokenStorage,
        ApiAggregatorForProductModelPostSaveEventSubscriber $apiAggregatorForProductModelPostSave,
        WarmupQueryCache $warmupQueryCache,
        LoggerInterface $logger,
        array $apiConfiguration,
        LoggerInterface $apiProductAclLogger,
        SecurityFacade $security,
        RemoveProductModelHandler $removeProductModelHandler,
        ValidatorInterface $validator,
        EntityRepository $entityRepository,
        FhirConnectorProductModelNormalizer $fhirConnectorProductModelNormalizer,
        ListProductsQueryValidator $listProductsQueryValidator,
        ListProductsQueryHandler $listProductsQueryHandler,
    ) {
        parent::__construct(
            $pqbFactory,
            $pqbSearchAfterFactory,
            $normalizer,
            $channelRepository,
            $offsetPaginator,
            $searchAfterPaginator,
            $updater,
            $factory,
            $saver,
            $router,
            $productModelValidator,
            $productModelAttributeFilter,
            $productModelRepository,
            $partialUpdateStreamResource,
            $listProductModelsQueryValidator,
            $listProductModelsQueryHandler,
            $connectorProductModelNormalizer,
            $getConnectorProductModels,
            $tokenStorage,
            $apiAggregatorForProductModelPostSave,
            $warmupQueryCache,
            $logger,
            $apiConfiguration,
            $apiProductAclLogger,
            $security,
            $removeProductModelHandler,
            $validator
        );
        $this->tokenStorage = $tokenStorage;
        $this->entityRepository = $entityRepository;
        $this->getConnectorProductModels = $getConnectorProductModels;
        $this->listProductModelsQueryValidator = $listProductModelsQueryValidator;
        $this->listProductModelsQueryHandler = $listProductModelsQueryHandler;
        $this->fhirConnectorProductModelNormalizer = $fhirConnectorProductModelNormalizer;
        $this->listProductsQueryValidator = $listProductsQueryValidator;
        $this->listProductsQueryHandler = $listProductsQueryHandler;
    }

    /**
     * @AclAncestor("pim_fhir_api_product_model_list")
     */
    public function listAction(Request $request): JsonResponse
    {
        $user = $this->tokenStorage->getToken()
            ->getUser();
        Assert::isInstanceOf($user, UserInterface::class);

        $query = new ListProductModelsQuery();

        //Query Fhir mappings
        $fhir_mapping = $this->entityRepository->findAll();
        $attributes = [];
        foreach ($fhir_mapping as $mapping) {
            $attributes[] = $mapping->getCode();
        }
        //Query fhir attributes
        $query->attributeCodes = $attributes;

        if ($request->query->has('locales')) {
            $query->localeCodes = explode(',', $request->query->get('locales'));
        }
        if ($request->query->has('search')) {
            $query->search = json_decode($request->query->get('search'), true);
            if (! is_array($query->search)) {
                throw new UnprocessableEntityHttpException('Search query parameter should be valid JSON.');
            }
        }
        $query->channelCode = $request->query->get('scope', null);
        $query->limit = $request->query->get('limit', $this->apiConfiguration['pagination']['limit_by_default']);
        $query->paginationType = $request->query->get('pagination_type', PaginationTypes::OFFSET);
        $query->searchLocaleCode = $request->query->get('search_locale', null);
        $query->withCount = $request->query->get('with_count', 'false');
        $query->page = $request->query->get('page', 1);
        $query->searchChannelCode = $request->query->get('search_scope', null);
        $query->searchAfter = $request->query->get('search_after', null);
        $query->userId = $user->getId();

        try {
            $this->listProductModelsQueryValidator->validate($query);
            $productModels = $this->listProductModelsQueryHandler->handle(
                $query
            ); // in try block as PQB is doing validation also
        } catch (InvalidQueryException $e) {
            if ($e->getCode() === 404) {
                throw new NotFoundHttpException($e->getMessage(), $e);
            }
            throw new UnprocessableEntityHttpException($e->getMessage(), $e);
        } catch (BadRequest400Exception $e) {
            $message = json_decode($e->getMessage(), true);

            if ($message !== null && isset($message['error']['root_cause'][0]['type'])
                && $message['error']['root_cause'][0]['type'] === 'illegal_argument_exception'
                && strpos(
                    $message['error']['root_cause'][0]['reason'],
                    'Result window is too large, from + size must be less than or equal to:'
                ) === 0) {
                throw new DocumentedHttpException(
                    Documentation::URL_DOCUMENTATION . 'pagination.html#search-after-type',
                    'You have reached the maximum number of pages you can retrieve with the "page" pagination type. Please use the search after pagination type instead',
                    $e
                );
            }

            throw new ServerErrorResponseException($e->getMessage(), $e->getCode(), $e);
        }

        return new JsonResponse($this->normalizeProductModelsList($productModels, $query));
    }

    private function normalizeProductModelsList(
        ConnectorProductModelList $connectorProductModels,
        ListProductModelsQuery $query
    ): array {
        $queryParameters = [
            'with_count' => $query->withCount,
            'pagination_type' => $query->paginationType,
            'limit' => $query->limit,
        ];

        if ($query->search !== []) {
            $queryParameters['search'] = json_encode($query->search);
        }
        if ($query->channelCode !== null) {
            $queryParameters['scope'] = $query->channelCode;
        }
        if ($query->searchChannelCode !== null) {
            $queryParameters['search_scope'] = $query->searchChannelCode;
        }
        if ($query->localeCodes !== null) {
            $queryParameters['locales'] = implode(',', $query->localeCodes);
        }
        if ($query->attributeCodes !== null) {
            $queryParameters['attributes'] = join(',', $query->attributeCodes);
        }

        $bundle = [];

        if ($query->paginationType === PaginationTypes::OFFSET) {
            $queryParameters = [
                'page' => $query->page,
            ] + $queryParameters;

            $paginationParameters = [
                'query_parameters' => $queryParameters,
                'list_route_name' => 'pim_fhir_api_product_model_list',
                'item_route_name' => 'pim_fhir_api_product_model_get',
                'item_identifier_key' => 'id',
            ];

            $count = $query->withCountAsBoolean() ? $connectorProductModels->totalNumberOfProductModels() : null;

            $paginated = $this->offsetPaginator->paginate(
                $this->fhirConnectorProductModelNormalizer->normalizeConnectorProductModelList($connectorProductModels),
                $paginationParameters,
                $count
            );
            //create fhir bundle output
            $bundle['resourceType'] = 'Bundle';
            if (array_key_exists('current_page', $paginated)) {
                $bundle['meta'] = [
                    'current_page' => $paginated['current_page'],
                ];
            }
            $bundle['type'] = 'searchset';
            $bundle['timestamp'] = date('c');
            $bundle['total'] = $connectorProductModels->totalNumberOfProductModels();
            foreach ($paginated['_links'] as $k => $l) {
                $bundle['link'][] = [
                    'relation' => $k,
                    'url' => $l['href'],
                ];
            }
            foreach ($paginated['_embedded']['items'] as $v) {
                $entry = [];
                $entry['fullUrl'] = $v['_links']['self']['href'];
                unset($v['_links']);
                $entry['resource'] = $v;
                $entry['search'] = 'match';
                $bundle['entry'][] = $entry;
            }

            return $bundle;
        }
        $productModels = $connectorProductModels->connectorProductModels();
        $lastProductModel = end($productModels);

        $parameters = [
            'query_parameters' => $queryParameters,
            'search_after' => [
                'next' => $lastProductModel !== false ? $lastProductModel->id() : null,
                'self' => $query->searchAfter,
            ],
            'list_route_name' => 'pim_fhir_api_product_model_list',
            'item_route_name' => 'pim_fhir_api_product_model_get',
            'item_identifier_key' => 'id',
        ];

        $paginated = $this->searchAfterPaginator->paginate(
            $this->fhirConnectorProductModelNormalizer->normalizeConnectorProductModelList($connectorProductModels),
            $parameters,
            null
        );
        //create fhir bundle output
        $bundle['resourceType'] = 'Bundle';
        if (array_key_exists('current_page', $paginated)) {
            $bundle['meta'] = [
                'current_page' => $paginated['current_page'],
            ];
        }
        $bundle['type'] = 'searchset';
        $bundle['timestamp'] = date('c');
        $bundle['total'] = $connectorProductModels->totalNumberOfProductModels();
        foreach ($paginated['_links'] as $k => $l) {
            $bundle['link'][] = [
                'relation' => $k,
                'url' => $l['href'],
            ];
        }
        foreach ($paginated['_embedded']['items'] as $v) {
            $entry = [];
            $entry['fullUrl'] = $v['_links']['self']['href'];
            unset($v['_links']);
            $entry['resource'] = $v;
            $entry['search'] = 'match';
            $bundle['entry'][] = $entry;
        }

        return $bundle;
    }
}
