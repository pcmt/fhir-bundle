<?php

declare(strict_types=1);

namespace PcmtFhirBundle\Controller\ExternalApi;

use Akeneo\Pim\Enrichment\Component\Product\Model\Product;
use Akeneo\Pim\Enrichment\Component\Product\Model\ProductModel;
use Akeneo\Pim\Enrichment\Component\Product\ProductModel\Query\GetConnectorProductModels;
use Akeneo\Pim\Enrichment\Component\Product\Query\GetConnectorProducts;
use Akeneo\Pim\Enrichment\Component\Product\Query\ProductQueryBuilderFactoryInterface;
use Akeneo\Pim\Enrichment\Component\Product\Repository\ProductRepositoryInterface;
use Akeneo\Tool\Component\Api\Pagination\PaginatorInterface;
use Akeneo\UserManagement\Bundle\Context\UserContext;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Oro\Bundle\DataGridBundle\Datagrid\ManagerInterface;
use Oro\Bundle\DataGridBundle\Datagrid\MetadataParser;
use PcmtFhirBundle\Normalizer\ExternalApi\ConnectorR5InventoryItemNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Copyright (c) 2022, VillageReach
 * Licensed under the Non-Profit Open Software License version 3.0.
 * SPDX-License-Identifier: NPOSL-3.0
 */
class Fhir5InventoryItemController
{
    protected ProductQueryBuilderFactoryInterface $pqbFactory;

    protected PaginatorInterface $offsetPaginator;

    protected array $apiConfiguration;

    protected MetadataParser $metadata;

    protected ManagerInterface $managerInterface;

    /**
     * @var NormalizerInterface
     */
    protected $normalizer;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var UserContext
     */
    protected $userContext;

    /**
     * @var ManagerInterface
     */
    protected $entityManager;

    private TokenStorageInterface $tokenStorage;

    /**
     * @var EntityRepository
     */
    private $entityRepository;

    /**
     * @var ConnectorR5InventoryItemNormalizer
     */
    private $fhir5InventoryItemNormalizer;

    /**
     * @var GetConnectorProducts
     */
    private $getConnectorProducts;

    /**
     * @var GetConnectorProductModels
     */
    private $getConnectorProductModels;

    public function __construct(
        ProductQueryBuilderFactoryInterface $pqbFactory,
        PaginatorInterface $offsetPaginator,
        TokenStorageInterface $tokenStorage,
        array $apiConfiguration,
        EntityRepository $entityRepository,
        ConnectorR5InventoryItemNormalizer $fhir5InventoryItemNormalizer,
        ManagerInterface $managerInterface,
        NormalizerInterface $normalizer,
        ProductRepositoryInterface $productRepository,
        UserContext $userContext,
        GetConnectorProducts $getConnectorProducts,
        GetConnectorProductModels $getConnectorProductModels,
        EntityManagerInterface $entityManager
    ) {
        $this->pqbFactory = $pqbFactory;
        $this->apiConfiguration = $apiConfiguration;
        $this->tokenStorage = $tokenStorage;
        $this->entityRepository = $entityRepository;
        $this->fhir5InventoryItemNormalizer = $fhir5InventoryItemNormalizer;
        $this->managerInterface = $managerInterface;
        $this->offsetPaginator = $offsetPaginator;
        $this->normalizer = $normalizer;
        $this->productRepository = $productRepository;
        $this->userContext = $userContext;
        $this->getConnectorProducts = $getConnectorProducts;
        $this->getConnectorProductModels = $getConnectorProductModels;
        $this->entityManager = $entityManager;
    }

    public function listAction(Request $request): JsonResponse
    {
        //create query for datagrid
        $query = [
            'product-grid' => [
                '_pager' => [
                    '_page' => $request->query->get('page', '1'),
                    '_per_page' => $request->query->get('perPage', '25'),
                ],
                '_filter' => [
                    'label_or_identifier' => [
                        'value' => $request->query->get('filter'),
                    ],
                ],
            ],
        ];

        $request->query->add($query);

        $grid = $this->managerInterface->getDatagrid('product-grid');

        $result = $grid->getData();
        $datas = empty($result) ? [] : $result->toArray()['data'];
        $connectorProductLists = [];
        foreach ($datas as $value) {
            $connectorProductLists[] = [
                'identifier' => $value['identifier'],
                'document_type' => $value['document_type'],
            ];
        }

        $bundle = [];
        $queryParameters = [
            'page' => $request->query->get('product-grid')['_pager']['_page'],
        ];
        $paginationParameters = [
            'query_parameters' => $queryParameters,
            'list_route_name' => 'pim_fhir5_api_inventory_item_list',
            'item_route_name' => 'pim_fhir5_api_inventory_item_get',
            'item_identifier_key' => 'id',
        ];

        $count = $result['totalRecords'] ?? 0;

        $paginated = $this->offsetPaginator->paginate(
            $this->fhir5InventoryItemNormalizer->normalizeConnectorInventory($connectorProductLists),
            $paginationParameters,
            $count
        );
        //create fhir bundle output
        $bundle['resourceType'] = 'Bundle';

        $bundle['type'] = 'searchset';
        $bundle['timestamp'] = date('c');
        $bundle['total'] = $count;
        foreach ($paginated['_links'] as $k => $l) {
            $bundle['link'][] = [
                'relation' => $k,
                'url' => $l['href'],
            ];
        }
        foreach ($paginated['_embedded']['items'] as $v) {
            $entry = [];
            $entry['fullUrl'] = $v['_links']['self']['href'];
            unset($v['_links']);
            $entry['resource'] = $v;
            $entry['search'] = 'match';
            $bundle['entry'][] = $entry;
        }

        return new JsonResponse($bundle);
    }

    /**
     * @return JsonResponse
     */
    public function getInventoryItem(Request $request, string $code)
    {
        $user = $this->tokenStorage->getToken()
            ->getUser();

        try {
            $productRepository = $this->entityManager->getRepository(Product::class);
            $productData = $productRepository->findOneBy([
                'inventory_item_uuid' => $code,
            ]);
            $product = $this->getConnectorProducts->fromProductIdentifier(
                $productData->getIdentifier(),
                $user->getId()
            );
        } catch (\Throwable $th) {
            $product = null;
        }

        try {
            $productModelRepository = $this->entityManager->getRepository(ProductModel::class);
            $productModelData = $productModelRepository->findOneBy([
                'inventory_item_uuid' => $code,
            ]);
            $productModel = $this->getConnectorProductModels->fromProductModelCode(
                $productModelData->getCode(),
                $user->getId()
            );
        } catch (\Throwable $th) {
            $productModel = null;
        }
        if ($product === null && $productModel === null) {
            throw new NotFoundHttpException(sprintf('InventoryItem with resource %s could not be found.', $code));
        }

        $normilizeInventoryItem = [];
        if ($product) {
            $normilizeInventoryItem = $this->fhir5InventoryItemNormalizer->normalizeConnectorProduct($product);
        }
        if ($productModel) {
            $normilizeInventoryItem = $this->fhir5InventoryItemNormalizer->normalizeConnectorProductModel(
                $productModel
            );
        }

        return new JsonResponse($normilizeInventoryItem);
    }
}
