<?php
/**
 * Copyright (c) 2022, VillageReach
 * Licensed under the Non-Profit Open Software License version 3.0.
 * SPDX-License-Identifier: NPOSL-3.0
 */

declare(strict_types=1);

namespace PcmtFhirBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use PcmtFhirBundle\Entity\FhirMapping;
use PcmtFhirBundle\Repository\FhirMappingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FhirController extends AbstractController
{
    private $entityManager;

    private $fhirMappingRepository;

    public function __construct(EntityManagerInterface $entityManager, FhirMappingRepository $fhirMappingRepository)
    {
        $this->entityManager = $entityManager;
        $this->fhirMappingRepository = $fhirMappingRepository;
    }

    public function createAction(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $fhir_mapping = null;

        if ($data['is_fhir5']) {
            $fhir_mapping = $this->fhirMappingRepository->findOneBy([
                'code' => $data['code'],
                'is_fhir5' => $data['is_fhir5'],
            ]);
        } else {
            $fhir_mapping = $this->fhirMappingRepository->findOneBy([
                'code' => $data['code'],
                'is_fhir5' => false,
            ]);
        }

        $success = false;
        $error = '';

        if (! $fhir_mapping) {
            try {
                // create
                $fhirMapping = new FhirMapping();
                $fhirMapping->setCode($data['code']);
                $fhirMapping->setType($data['type']);
                $fhirMapping->setMapping($data['mapping']);
                $fhirMapping->setCodableConcepts((array) $data['codable_concepts']);
                $fhirMapping->setIsFhir5($data['is_fhir5']);

                $this->entityManager->persist($fhirMapping);
                $this->entityManager->flush();

                $success = true;
            } catch (\Throwable $e) {
                $error = $e->getMessage();
            }
        } else {
            try {
                // update
                $fhir_mapping->setMapping($data['mapping']);
                $fhir_mapping->setType($data['type']);
                $fhir_mapping->setCodableConcepts($data['codable_concepts'] ?? []);
                $fhir_mapping->setIsFhir5($data['is_fhir5']);

                $this->entityManager->persist($fhir_mapping);
                $this->entityManager->flush();

                $success = true;
            } catch (\Throwable $e) {
                $error = $e->getMessage();
            }
        }

        return new JsonResponse([
            'success' => $success,
            'error' => $error,
        ]);
    }

    public function currentMappingAction(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $fhir_mapping = null;
        if ($data['is_fhir5']) {
            $fhir_mapping = $this->fhirMappingRepository->findOneBy([
                'code' => $data['code'],
                'is_fhir5' => $data['is_fhir5'],
            ]);
        } else {
            $fhir_mapping = $this->fhirMappingRepository->findOneBy([
                'code' => $data['code'],
                'is_fhir5' => false,
            ]);
        }

        $mapping = '';
        $codable_concepts = '';
        $found = false;

        if ($fhir_mapping) {
            $mapping = $fhir_mapping->getMapping();
            $codable_concepts = $fhir_mapping->getCodableConcepts();
            $found = true;
        }

        return new JsonResponse([
            'found' => $found,
            'mapping' => $mapping,
            'codable_concepts' => $codable_concepts,
            'is_fhir5' => $data['is_fhir5'],
        ]);
    }
}
