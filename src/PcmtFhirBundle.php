<?php

declare(strict_types=1);

/**
 * Copyright (c) 2022, VillageReach
 * Licensed under the Non-Profit Open Software License version 3.0.
 * SPDX-License-Identifier: NPOSL-3.0
 */

namespace PcmtFhirBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PcmtFhirBundle extends Bundle
{
}
