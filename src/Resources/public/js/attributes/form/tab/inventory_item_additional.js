'use strict';


define(['jquery', 'underscore', 'oro/translator', 'pim/form', 'pcmt/fhir/template/attribute/tab/inventory_item', 'oro/messenger', 'routing', 'pim/fetcher-registry'],
    function ($, _, __, BaseForm, template, messenger, Routing, FetcherRegistry) {
        return BaseForm.extend({
            events: {
                "change #fhir_mapping": "updateModelAfterChange",
                "submit #fhir-mapping-form": "submitForm", // Bind form submission
                "click .add-more-btn": "addMoreFields",
                "click .remove-field-group-btn": "removeFieldGroup"  // Remove field group
            },

            template: _.template(template),
            mapping_value: '',
            field_hidden: null,
            fields: [],
            initialize(meta) {
                this.config = meta.config;
                this.context = this.getContextOptions(); // Set the context based on options
                BaseForm.prototype.initialize.apply(this, arguments);
            },

            configure: function () {
                this.trigger('tab:register', {
                    code: this.code,
                    label: __('pcmt_core.fhir5.tab.lable')
                });

                this.listenTo(this.getRoot(), 'pcmt:fhir:attribute:form:render:before', () => {
                    this.getCurrentMapping();
                });

                return BaseForm.prototype.configure.apply(this, arguments);
            },

            render: function () {

                this.getRoot().trigger('pcmt:fhir:attribute:form:render:before');

                $.ajax({
                    url: Routing.generate('pim_fhir_get_mapping'),
                    type: 'POST',
                    data: JSON.stringify({
                        code: this.getFormData().code,
                        type: this.getFormData().type,
                        mapping: this.mapping_value,
                        is_fhir5: true
                    })
                }).done(function (resp) {
                    this.mapping_value = resp.mapping;
                    this.fields = resp.codable_concepts || []; // Ensure `fields` is updated


                    this.$el.html(this.template({
                        label: __('pcmt_core.fhir5.select.lable'),
                        options: this.getMappingOptions(),
                        selected: resp.mapping,  // Use updated mapping_value
                        title: __('pcmt_core.fhir5.title.lable'),
                        placeholder: __('pcmt_core.fhir5.placeholder.lable'),
                        fields: this.fields,  // Use updated fields from server
                        submitUrl: Routing.generate('pim_fhir_fhir_create'),
                        addMoreLabel: __('pcmt_core.fhir5.add_more.lable')
                    }));

                    // Initialize select2
                    this.$('.select2').select2();

                    this.delegateEvents();
                    this.renderExtensions();

                    // Toggle visibility of fields based on updated mapping_value
                    this.toggleFieldsVisibility();

                    this.reIndexFields();  // Ensure fields are correctly indexed

                    return this;

                }.bind(this)).fail(function () {
                    console.error("Failed to get current mapping");
                });
              
            },

            updateModelAfterChange: function (event) {
                this.mapping_value = event.target.value;
                this.toggleFieldsVisibility();
                this.getCurrentMapping();
                return this;
            },

            toggleFieldsVisibility: function () {
                if (this.context.includes(this.mapping_value)) {
                    this.field_hidden = false;
                    this.showFields();
                } else {
                    this.field_hidden = true;
                    this.hideFields();
                }
            },

            showFields: function () {
                this.$(".coduble_concept_container").show();
            },

            hideFields: function () {
                this.$(".coduble_concept_container").hide();
            },

            getCurrentMapping:  function () {
                $.ajax({
                    url: Routing.generate('pim_fhir_get_mapping'),
                    type: 'POST',
                    data: JSON.stringify({
                        code: this.getFormData().code,
                        type: this.getFormData().type,
                        mapping: this.mapping_value,
                        is_fhir5: true
                    })
                }).done(function (resp) {
                    this.mapping_value = resp.mapping;
                    this.fields = resp.codable_concepts || []; // Ensure `fields` is updated


                }.bind(this)).fail(function () {
                    console.error("Failed to get current mapping");
                });

                //used for getting attribute details
                FetcherRegistry.getFetcher('attribute').fetchByIdentifiers(this.getFormData().code).then(
                    function (attributes) {
                        this.attributeProperty = attributes[0];
                    }.bind(this)
                );
            },

            getMappingOptions: function () {
                return [
                    { "value": "baseUnit", "label": __("fhir5.options.r5baseUnit"), "codeableConcept": true },
                    { "value": "netContent", "label": __("fhir5.options.r5netContent"), "codeableConcept": false },
                    { "value": "description", "label": __("fhir5.options.r5description"), "codeableConcept": false },
                    { "value": "identifier", "label": __("fhir5.options.r5identifier"), "codeableConcept": false },
                    { "value": "name", "label": __("fhir5.options.r5name"), "codeableConcept": false },
                    { "value": "characteristic", "label": __("fhir5.options.r5characteristic"), "codeableConcept": true },
                ];
            },

            getContextOptions: function () {
                const options = this.getMappingOptions();
                return options
                    .filter(option => option.codeableConcept)
                    .map(option => option.value);
            },

            submitForm: function (event) {
                event.preventDefault();

                const formDataArray = this.$('#fhir-mapping-form').serializeArray();

                const formDataObject = {};
                formDataArray.forEach((item) => {
                    formDataObject[item.name] = this.field_hidden ? '' : item.value;
                });

                formDataArray.forEach(item => {
                    if (item.name === 'system_url[]') {
                        formData.system_url.push(item.value);
                    }
                    if (item.name === 'codable_code[]') {
                        formData.codable_code.push(item.value);
                    }
                });

                // Validate form data
                const validationErrors = this.validateForm(formDataObject);

                if (Object.keys(validationErrors).length > 0) {
                    this.displayValidationErrors(validationErrors);
                    return;
                } else{
                    if (this.$('#fhir_mapping').val() == 'netContent' && this.attributeProperty['type'] != 'pim_catalog_metric') {
                        messenger.notify(
                            'error',
                            __('pcmt_core.fhir5.errors.net_content')
                        );
                        return;
                    }
                    // Create a structured form data object
                    const formData = {
                        is_fhir5: true,
                        code: this.getFormData().code,
                        type: this.getFormData().type,
                        mapping: this.$('#fhir_mapping').val(),
                        codable_concepts: this.field_hidden == true ? [] : this.groupRepeatableFields(formDataObject),
                    };
                    $.ajax({
                        url: event.currentTarget.action,  // Get the form action URL
                        type: 'POST',
                        contentType: 'application/json',
                        data: JSON.stringify(formData),
                    }).done(function (resp) {
                        if (resp.success === true) {
                            messenger.notify(
                                'success',
                                __('pcmt_core.fhir5.saved.lable')
                            );
                        } else {
                            messenger.notify(
                                'error',
                                __('pcmt_core.fhir5.failed.lable')
                            );
                        }
                    }).fail(function () {
                        messenger.notify(
                            'error',
                            __('pcmt_core.fhir5.failed.lable')
                        );
                    });
                }

            },

            validateForm: function (formData) {
                const errors = {};


                if (this.isStringEmpty(this.$('#fhir_mapping').val())) {
                    errors.fhir_mapping = __('pcmt_core.fhir5.errors.required', { field: __('pcmt_core.fhir5.fields.fhir_mapping') });
                }

                if (!this.field_hidden) {
                    const systemUrls = [];
                    const codableCodes = [];

                    Object.keys(formData).forEach(key => {
                        if (key.startsWith("system_url[")) {
                            systemUrls.push(formData[key]);
                        }
                        if (key.startsWith("codable_code[")) {
                            codableCodes.push(formData[key]);
                        }
                    });
                    if (Array.isArray(systemUrls)) {
                        systemUrls.forEach((url, index) => {
                            if (this.isStringEmpty(url)) {
                                errors[`system_url[${index}]`] = __('pcmt_core.fhir5.errors.required', { field: __('pcmt_core.fhir5.fields.system_url') });
                            }
                        });
                    }
                    if (Array.isArray(codableCodes)) {
                        codableCodes.forEach((code, index) => {
                            if (this.isStringEmpty(code)) {
                                errors[`codable_code[${index}]`] = __('pcmt_core.fhir5.errors.required', { field: __('pcmt_core.fhir5.fields.codable_code') });
                            }
                        });
                    }
                }
                console.log(errors);
                

                return errors;
            },

            displayValidationErrors: function (errors) {
                this.$('.AknFieldContainer').find('.AknFieldContainer-validationError').remove();

                Object.keys(errors).forEach(function (field) {
                    const errorMessage = errors[field];

                    const match = field.match(/(\w+)\[(\d+)\]/); 
                    if (match) {
                        const fieldName = match[1]; 
                        const index = match[2];     

                        const $fieldContainer = this.$(`.dynamic-field-group[data-index="${index}"] .AknFieldContainer[data-code="${fieldName}"]`);

                        $fieldContainer.append(`<div class="AknFieldContainer-validationError">${errorMessage}</div>`);
                    } else {
                        const $fieldContainer = this.$(`.AknFieldContainer[data-code="${field}"]`);
                        $fieldContainer.append(`<div class="AknFieldContainer-validationError">${errorMessage}</div>`);
                    }
                }.bind(this));
            },
            isStringEmpty: function (str) {
                console.log("string", str);

                if (typeof str !== 'string') {
                    throw new TypeError('Argument must be a string');
                }
                return str.trim() === '';
            }

            ,
            addMoreFields: function () {
                const $lastFieldGroup = this.$('.dynamic-field-group').last();
                const $newFieldGroup = $lastFieldGroup.clone();

                $newFieldGroup.find('input').val('');

                const newIndex = this.$('.dynamic-field-group').length;
                $newFieldGroup.attr('data-index', newIndex);

                this.$('.repeatable-field-container').append($newFieldGroup);

                this.reIndexFields();
            },
            removeFieldGroup: function (event) {
                const $button = $(event.currentTarget);
                const $container = this.$('.repeatable-field-container');

                if ($container.find('.dynamic-field-group').length > 1) {
                    $button.closest('.dynamic-field-group').remove();
                    this.reIndexFields();
                }
                this.reIndexFields();
            },

            reIndexFields: function () {
                const $container = this.$('.repeatable-field-container');

                $container.find('.dynamic-field-group').each(function (index) {
                    $(this).attr('data-index', index);

                    $(this).find('input').each(function () {
                        let name = $(this).attr('name');

                        let updatedName = name.replace(/\[\d*\]/, '[' + index + ']');

                        $(this).attr('name', updatedName);
                    });
                });
            },
            groupRepeatableFields(formData) {
                const codableConcept = [];

                const systemUrls = Object.keys(formData)
                    .filter(key => key.startsWith('system_url'))
                    .map(key => formData[key]);

                const codableCodes = Object.keys(formData)
                    .filter(key => key.startsWith('codable_code'))
                    .map(key => formData[key]);

                systemUrls.forEach((system_url, index) => {
                    codableConcept.push({
                        system_url: system_url,
                        codable_code: codableCodes[index]
                    });
                });

                return codableConcept;
            }


        });
    }
    
);
