<?php
/**
 * Copyright (c) 2022, VillageReach
 * Licensed under the Non-Profit Open Software License version 3.0.
 * SPDX-License-Identifier: NPOSL-3.0
 */

declare(strict_types=1);

namespace PcmtFhirBundle\Entity;

class FhirMapping
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $mapping;

    private $codable_concepts;

    /**
     * @var string
     */
    private $is_fhir5;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): object
    {
        $this->id = $id;

        return $this;
    }

    public function setCode(string $code): object
    {
        $this->code = $code;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setType(string $type): object
    {
        $this->type = $type;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setMapping(string $mapping): object
    {
        $this->mapping = $mapping;

        return $this;
    }

    public function getMapping(): string
    {
        return $this->mapping;
    }

    public function setCodableConcepts(array $codableConcepts): self | null
    {
        $this->codable_concepts = $codableConcepts ?? [];

        return $this;
    }

    public function getCodableConcepts(): ?array
    {
        return $this->codable_concepts;
    }

    public function setIsFhir5($is_fhir5)
    {
        $this->is_fhir5 = $is_fhir5;
        return $this;
    }

    public function getIsFhir5()
    {
        return $this->is_fhir5;
    }
}
