<?php

/**
 * Copyright (c) 2022, VillageReach
 * Licensed under the Non-Profit Open Software License version 3.0.
 * SPDX-License-Identifier: NPOSL-3.0
 */

declare(strict_types=1);

namespace PcmtFhirBundle\Normalizer\ExternalApi;

use Akeneo\Pim\Enrichment\Component\Product\Connector\ReadModel\ConnectorProduct;
use Akeneo\Pim\Enrichment\Component\Product\Connector\ReadModel\ConnectorProductModel;
use Akeneo\Pim\Enrichment\Component\Product\Model\Product;
use Akeneo\Pim\Enrichment\Component\Product\Model\ProductModel;
use Akeneo\Pim\Enrichment\Component\Product\Normalizer\ExternalApi\ValuesNormalizer;
use Akeneo\Pim\Enrichment\Component\Product\ProductModel\Query\GetConnectorProductModels;
use Akeneo\Pim\Enrichment\Component\Product\Query\GetConnectorProducts;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use PcmtFhirBundle\Service\Helper\FHIR5MapHelper;
use PcmtFhirBundle\Service\Helper\FHIRMappingKey;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ConnectorR5InventoryItemNormalizer
{
    /**
     * @var ValuesNormalizer
     */
    private $valuesNormalizer;

    /**
     * @var EntityRepository
     */
    private $entityRepository;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var ObjectRepository
     */
    private $categoryRepository;

    /**
     * @var ObjectRepository
     */
    private $assosationTypeRepository;

    /**
     * @var FHIR5MapHelper
     */
    private $fHIR5MapHelper;

    /**
     * @var GetConnectorProducts
     */
    private $getConnectorProducts;

    private GetConnectorProductModels $getConnectorProductModels;

    private TokenStorageInterface $tokenStorage;

    /**
     * @var ManagerInterface
     */
    private $entityManager;

    public function __construct(
        ValuesNormalizer $valuesNormalizer,
        EntityRepository $entityRepository,
        UrlGeneratorInterface $router,
        ObjectRepository $categoryRepository,
        ObjectRepository $assosationTypeRepository,
        FHIR5MapHelper $fHIR5MapHelper,
        GetConnectorProducts $getConnectorProducts,
        GetConnectorProductModels $getConnectorProductModels,
        TokenStorageInterface $tokenStorage,
        EntityManagerInterface $entityManager,
    ) {
        $this->valuesNormalizer = $valuesNormalizer;
        $this->entityRepository = $entityRepository;
        $this->router = $router;
        $this->categoryRepository = $categoryRepository;
        $this->assosationTypeRepository = $assosationTypeRepository;
        $this->fHIR5MapHelper = $fHIR5MapHelper;
        $this->getConnectorProducts = $getConnectorProducts;
        $this->getConnectorProductModels = $getConnectorProductModels;
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
    }

    public function normalizeConnectorInventory($inventoryList): array
    {
        $normalizedProducts = [];
        $normilizedProductModels = [];

        $user = $this->tokenStorage->getToken()
            ->getUser();

        foreach ($inventoryList as $value) {
            if ($value['document_type'] === 'product') {
                $product = $this->getConnectorProducts->fromProductIdentifier($value['identifier'], $user->getId());
                $normalizedProducts[] = $this->normalizeConnectorProduct($product);
            } elseif ($value['document_type'] === 'product_model') {
                $productModel = $this->getConnectorProductModels->fromProductModelCode(
                    $value['identifier'],
                    $user->getId()
                );
                $normilizedProductModels[] = $this->normalizeConnectorProductModel($productModel);
            }
        }
        return array_merge($normilizedProductModels, $normalizedProducts);
    }

    public function normalizeConnectorProductModel(ConnectorProductModel $connectorProductModel): array
    {
        $productModelRepository = $this->entityManager->getRepository(ProductModel::class);
        $productModelUuid = $productModelRepository->findOneById($connectorProductModel->id());
        $values = $this->valuesNormalizer->normalize($connectorProductModel->values(), 'standard');
        $attr_values = $connectorProductModel->attributeCodesInValues();

        $identifier = [];
        $description = [];
        $name = [];
        $codableConcept = [];
        $netContent = [];
        $product_model_route = $this->router->generate(
            'pim_fhir5_api_inventory_item_get',
            [
                'code' => $productModelUuid->inventory_item_uuid,
            ],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        $fhir_mappings =
        $this->entityRepository->createQueryBuilder('e')
            ->where('e.is_fhir5 = :is_fhir5')
            ->setParameter('is_fhir5', true)
            ->getQuery()
            ->getResult();

        foreach ($fhir_mappings as $mapping) {
            if ($mapping) {
                $attribute_route = $this->router->generate(
                    'pim_api_attribute_get',
                    [
                        'code' => $mapping->getCode(),
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
                if (! empty($mapping->getCodableConcepts())) {
                    $codableConcept[$mapping->getMapping()] = $this->fHIR5MapHelper->mapCodableCode(
                        $mapping->getCodableConcepts(),
                        $values,
                        $mapping->getCode()
                    );
                } else {
                    switch ($mapping->getMapping()) {
                        case FHIRMappingKey::Identifier:
                            $identifier[] = $this->fHIR5MapHelper->mapIdentifier(
                                $attribute_route,
                                $mapping->getCode(),
                                $product_model_route,
                                $values
                            );
                            break;
                        case FHIRMappingKey::Description:

                            $description[] = $this->fHIR5MapHelper->mapDescription($mapping->getCode(), $values);
                            break;
                        case FHIRMappingKey::BaseUnit:
                            $baseUnit[] = [
                                'system' => $mapping->getSystemUrl(),
                                'code' => $mapping->getCodableCode(),
                            ];
                            break;
                        case FHIRMappingKey::NetContent:
                            $netContent = [
                                'value' => $values[$mapping->getCode()][0]['data']['amount'],
                                'unit' => isset($values[$mapping->getCode()][0]['data']['unit']) ? $values[$mapping->getCode()][0]['data']['unit'] : '',
                            ];

                            break;
                        case FHIRMappingKey::Name:
                            $name[] = $this->fHIR5MapHelper->mapName(
                                $attribute_route,
                                $mapping->getCode(),
                                $values,
                                FHIRMappingKey::Preferred
                            );
                            break;
                    }
                }
            }
        }

        $result = [
            'resourceType' => 'InventoryItem',
            'id' => $productModelUuid->inventory_item_uuid,
            'code' => [
                'coding' => [
                    [
                        'system' => $product_model_route,
                        'code' => $connectorProductModel->code(),
                        'display' => $connectorProductModel->code(),
                    ],
                ],
                'text' => $connectorProductModel->code(),
            ],
            'identifier' => $identifier,
            'name' => $name,
            'text' => [
                'status' => 'generated',
                'div' => '<div xmlns="http://www.w3.org/1999/xhtml"><div class="hapiHeaderText"> <b>' . $connectorProductModel->code() . ' </b></div></div>',
            ],
            'status' => 'active',
            'category' => $this->fHIR5MapHelper->mapCategories(
                $this->categoryRepository->findByCode($connectorProductModel->categoryCodes())
            ),
            'netContent' => $netContent,
            'description' => $description,
            'association' => $this->fHIR5MapHelper->mapAssociations($connectorProductModel->associations()),
        ];

        $this->removeEmptyValues($result);
        return array_merge($result, $codableConcept);
    }

    public function normalizeConnectorProduct(ConnectorProduct $connectorProduct): array
    {
        $productRepository = $this->entityManager->getRepository(Product::class);
        $productUuid = $productRepository->findOneById($connectorProduct->id());
        $values = $this->valuesNormalizer->normalize($connectorProduct->values(), 'standard');
        $attr_values = $connectorProduct->attributeCodesInValues();
        $identifier = [];
        $baseUnit = [];
        $codableConcept = [];
        $description = [];
        $netContent = [];

        $product_route = $this->router->generate(
            'pim_fhir5_api_inventory_item_get',
            [
                'code' => $productUuid->inventory_item_uuid,
            ],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        $fhir_mappings =
        $this->entityRepository->createQueryBuilder('e')
            ->where('e.is_fhir5 = :is_fhir5')
            ->setParameter('is_fhir5', true)
            ->getQuery()
            ->getResult();
        $filteredMappings = array_filter($fhir_mappings, function ($mapping) use ($attr_values) {
            return in_array($mapping->getCode(), $attr_values, true);
        });

        foreach ($filteredMappings as $mapping) {
            if ($mapping) {
                $attribute_route = $this->router->generate(
                    'pim_api_attribute_get',
                    [
                        'code' => $mapping->getCode(),
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
                if (! empty($mapping->getCodableConcepts())) {
                    $codableConcept[$mapping->getMapping()] = $this->fHIR5MapHelper->mapCodableCode(
                        $mapping->getCodableConcepts(),
                        $values,
                        $mapping->getCode()
                    );
                } else {
                    switch ($mapping->getMapping()) {
                        case FHIRMappingKey::Identifier:
                            $identifier[] = $this->fHIR5MapHelper->mapIdentifier(
                                $attribute_route,
                                $mapping->getCode(),
                                $product_route,
                                $values
                            );
                            break;
                        case FHIRMappingKey::Description:
                            $description[] = $this->fHIR5MapHelper->mapDescription($mapping->getCode(), $values);
                            break;
                        case FHIRMappingKey::BaseUnit:
                            $baseUnit[] = [
                                'system' => $mapping->getSystemUrl(),
                                'code' => $mapping->getCodableCode(),
                            ];
                            break;
                        case FHIRMappingKey::NetContent:
                            $netContent = [
                                'value' => $values[$mapping->getCode()][0]['data']['amount'],
                                'unit' => isset($values[$mapping->getCode()][0]['data']) ? $values[$mapping->getCode()][0]['data']['unit'] : '',
                            ];

                            break;
                        case FHIRMappingKey::Name:
                            $name[] = $this->fHIR5MapHelper->mapName(
                                $attribute_route,
                                $mapping->getCode(),
                                $values,
                                FHIRMappingKey::TradeName
                            );
                            break;
                    }
                }
            }
        }

        $result = [
            'resourceType' => 'InventoryItem',
            'id' => $productUuid->inventory_item_uuid,
            'code' => [
                [
                    'system' => $product_route,
                    'code' => $connectorProduct->identifier(),
                    'display' => $connectorProduct->identifier(),
                ],
            ],
            'identifier' => $identifier,
            'text' => [
                'status' => 'generated',
                'div' => '<div xmlns="http://www.w3.org/1999/xhtml"><div class="hapiHeaderText"> <b>' . $connectorProduct->identifier() . ' </b></div></div>',
            ],
            'name' => $name,
            'status' => $connectorProduct->enabled() ? 'active' : '',
            'category' => $this->fHIR5MapHelper->mapCategories(
                $this->categoryRepository->findByCode($connectorProduct->categoryCodes())
            ),
            'netContent' => $netContent,
            'description' => $description,
            'association' => $this->fHIR5MapHelper->mapAssociations($connectorProduct->associations()),
        ];

        $this->removeEmptyValues($result);

        return array_merge($result, $codableConcept);
    }

    // Function to recursively remove empty values from an array
    public function removeEmptyValues(&$array)
    {
        foreach ($array as $key => &$value) {
            // If the value is an array, recursively call the function
            if (is_array($value)) {
                $this->removeEmptyValues($value);
                // If the array is empty after recursion, unset it
                if (empty($value)) {
                    unset($array[$key]);
                }
            }
            // If the value is empty, unset the key
            elseif (empty($value)) {
                unset($array[$key]);
            }
        }
    }
}
