# PCMT Fhir Bundle for Akeneo Pim

## Key features

FHIR product & item API. 

## Development
### Running Test-Suits
The PcmtFhirBundle is covered with tests and every change and addition has also to be covered with unit tests. It uses PHPUnit.

To run the tests you have to change to this project's root directory and run the following commands in your console:

```
make unit
```

### Coding style
PcmtFhirBundle the coding style can be checked with Easy Coding Standard.

```
make ecs
```
