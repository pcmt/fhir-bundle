# FHIR R5 Examples

Status:  These are WIP examples

## InventoryItem

### Simplest, and not terribly useful.

`GET http://pcmtUrl/fhirPath/r5/InventoryItem/123`

```json
{
  "resourceType": "InventoryItem",
  "id": "123",
  "status": "active"
}
```

### Product Model (aka Product) w/ name

`GET http://pcmtUrl/fhirPath/r5/InventoryItem/123`

```json
{
  "resourceType": "InventoryItem",
  "id": "123",
  "status": "active",
  "name": [
    {
      "nameType": {
        "uri": "http://hl7.org/fhir/ValueSet/inventoryitem-nametype",
        "code": "preferred"
      },
      "language": "en",
      "name": "Respiratior Mask, N95/FFP2, Medium"
    }
  ]
}
```

### Product (aka Item or Trade Item) w/ GTIN _012345678_

`GET http://pcmtUrl/fhirPath/r5/InventoryItem/1234`

```json
{
  "resourceType": "InventoryItem",
  "id": "1234",
  "status": "active",
  "identifier": [
    {
      "system": "https://www.gs1.org/gtin",
      "value": "012345678"
    }
  ]
}
```

### Product (aka Item or Trade Item) w/ association to parent product model

Note this also shows that in this catalog, the Item can inherit a parent's
name (_Respiratior Mask, N95/FFP2, Medium_).

`GET http://pcmtUrl/fhirPath/r5/InventoryItem/1234`

```json
{
  "resourceType": "InventoryItem",
  "id": "1234",
  "status": "active",
  "identifier": [
    {
      "system": "https://www.gs1.org/gtin",
      "value": "012345678"
    }
  ],
  "name": [
    {
      "nameType": {
        "uri": "http://hl7.org/fhir/ValueSet/inventoryitem-nametype",
        "code": "preferred"
      },
      "language": "en",
      "name": "Respiratior Mask, N95/FFP2, Medium"
    },
    {
      "nameType": {
        "uri": "http://hl7.org/fhir/ValueSet/inventoryitem-nametype",
        "code": "trade-name"
      },
      "language": "en",
      "name": "ACME Co N95, size Medium"
    }
  ],
  "association": [
    {
      "associationType": {
        "text": "catalog parent"
      },
      "relatedItem": {
        "reference": "GET http://pcmtUrl/fhirPath/r5/InventoryItem/123"
      }
    }
  ]
}
```

### Association (non-heirarchy)

For associations that come from PCMT's "Association" screen of a Product / Model, we can reference the 
Akeneo API as the Association Types have both a code and a display value.

`GET http://pcmtUrl/fhirPath/r5/InventoryItem/1234`

```json
{
  "resourceType": "InventoryItem",
  "id": "1234",
  "status": "active",
  "identifier": [
    {
      "system": "https://www.gs1.org/gtin",
      "value": "012345678"
    }
  ],
  "association": [
    {
      "associationType": {
        "coding": [
          {
            "system": "<PCMT_BASE>/api/rest/v1/association-types",
            "code": "SUBSTITUITION",
            "display": "Substituition"
          }
        ]
      },
      "relatedItem": {
        "reference": "GET http://pcmtUrl/fhirPath/r5/InventoryItem/123"
      },
      "quantity": {
        "numerator": 1,
        "denominator": 1
      }
    }
  ]
}
```

### Open questions

1. In a catalog of InventoryItems, it's a common expression to relate the
   concept of a product with it's own attributes (such as country, supplier,
   manufacturer codings), to items (those on a shelf).  It appears we'd use
   `association`, however it leaves open which coding system to use for 
   `associationType`.  
2. In `name`, what does a `nameType` of `original` mean?  In a catalog it could
  be the catalog's original name, however `prefered` seems to work better for
  that.  `trade-name` appears to work well for TradeItems, so what's `original`
  for?  URL: https://www.hl7.org/fhir/r5/valueset-inventoryitem-nametype.html
    - alternatively we could use `description` instead of `name` for catalog
      descriptions of a "model" concept.  The cardnality of `description`
      is at most 1 - which may not be a big issue.  At present it's a bit
      confusing how a catalog would use `description` seperate from `name`.